import csv 
import sys
import math

from feat import characteristic_features, characteristic_factors

def gen_primes(amount):
    primes = list()
    count = 3

    while len(primes) < amount:
        isPrime = True

        for x in range(2, int(math.sqrt(count) + 1)):
            if count % x == 0:
                isPrime = False
                break
        if isPrime:
            primes.append(count)

        count += 1

    return primes

def gen_grape_dict(grapes):
    primes = gen_primes(len(grapes))
    grape_id = dict()

    for g in grapes:
        grape_id[g] = primes.pop(0)

    return grape_id

def get_grape_val(grapes, grape_dict):
    val = 0

    for g in grapes.split(","):
        val *= int(grape_dict.get(g.strip(),1))

    return val

def gen_id_dict(data):
    ids = dict()
    counter = 1

    for d in data:
        ids[d] = counter
        counter += 1

    return ids

def gen_vec_from_characteristics(characteristics):

    cha_vec = dict()

    for ch in characteristics.split(","):        
        f = 1.0
        ch = ch.strip().lower()
        cha = ch
    
        if " " in ch:
            (fac, cha) = ch.split(" ")
            (id, f) = characteristic_factors[fac]
       
        if cha:
            (cat, val) = characteristic_features[cha]
            curr = cha_vec.get(cat, 0)
            curr += float(val) * float(f)
            cha_vec[cat] = curr

    return cha_vec


    
feature_names = ["hinta", "litrahinta", "valmistusmaa", "alue", "vuosikerta", "rypäleet", "luonnehdinta", "alkoholi-%", "hapot g/l"]
feature_data = dict()

country = set()
region = set()
grapes = set()
characteristics = set()

with open(sys.argv[1], 'r') as f:
    reader = csv.reader(f, delimiter = '\t')
    rownum = 0
    header = dict()
    for row in reader:
        if rownum == 0:
            header = { v.lower():i for i,v in enumerate(row) }
            rownum = rownum + 1
        else:
            if row[header["tyyppi"]] == "punaviinit":
                wine_name = row[header["nimi"]]
                flist = list()
                for feature in feature_names:

                    if feature == "valmistusmaa":
                        country.add(row[header[feature]].strip().lower())
                    elif feature == "alue":
                        reg = row[header[feature]].strip().lower()
                        if len(reg) > 0: region.add(reg) 
                    elif feature == "rypäleet":
                        for g in row[header[feature]].split(','):
                            if len(g.strip()) > 0: grapes.add(g.strip().lower()) 
                    elif feature == "luonnehdinta":
                        for c in row[header[feature]].split(','):
                            if len(c.strip()) > 0: characteristics.add(c.strip().lower()) 

                    flist.append(row[header[feature]].strip().lower())

                feature_data[wine_name] = flist

print(len(country))
print(len(region))
print(len(grapes))
print (len(characteristics))

grape_dict = gen_grape_dict(grapes)
country_dict = gen_id_dict(country)
region_dict = gen_id_dict(region)
counter = 1
wine_dict = dict()
features = list()

for name, data in feature_data.items():
    f = list()
    wine_dict[counter] = name
    f.append(counter)
    counter += 1
    
    f.append(float(data[0].replace(",",".")))
    f.append(float(data[1].replace(",",".")))
    f.append(country_dict[data[2]])
    f.append(region_dict.get(data[3],0))    
    if data[4].strip():
        f.append(int(data[4]))
    else: 
        f.append(0)
    f.append(get_grape_val(data[5], grape_dict))

    chars = gen_vec_from_characteristics(data[6])

    for i in range(1,17):
        f.append(chars.get(i,0))

    f.append(float(data[7].replace(",",".")))
    if data[8]:
        f.append(float(data[8].replace(",",".")))
    else:
        f.append(0)
    features.append(f)


#write data
with open("wines.txt","w") as fh:
    for i in range(1, counter):
        print('{0}:{1}'.format(i, wine_dict[i]), file = fh)

with open("features.txt","w") as fh:
    writer = csv.writer(fh, delimiter=":")
    for f in features:
        writer.writerow(f)
